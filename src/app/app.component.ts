import { Component, Output } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'recipe-book';
  loadedFeature ='recipe';

  onNavigate(featureToNavigate:string){
    this.loadedFeature=featureToNavigate;
  }
}
