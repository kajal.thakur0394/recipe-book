import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from '../../recipe.model';
import { RecipeService } from '../../recipes.service';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css'],
  //providers:[RecipeService]
})
export class RecipeItemComponent implements OnInit {
  @Input() recipe!: Recipe;
  @Input()index!: number;
  //@Output() recipeItemSelected=new EventEmitter<void>();
  // constructor(private recipeService: RecipeService) { }

  ngOnInit(): void {

  }
  // onSelect(){
  //   //this.recipeItemSelected.emit();
  //   this.recipeService.recipeSelected.emit(this.recipe);
  // }
}
