import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipes.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css'],
  //providers: [RecipeService]
})
export class RecipeListComponent implements OnInit {

  // @Output() recipeWasSelected = new EventEmitter<Recipe>();
  recipes!: Recipe[];
  // recipes: Recipe[] = [
  //   new Recipe("Vadapav", "Indian burger", "https://www.cookwithmanali.com/wp-content/uploads/2018/04/Vada-Pav.jpg"),
  //   new Recipe("Chicken Biryani", "All time bae", "https://www.pavaniskitchen.com/wp-content/uploads/2021/02/Biryani-1.jpg")
  // ];
  constructor(private recipeService: RecipeService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.recipes = this.recipeService.getRecipes();
  }
  // onRecipeSelected(recipe: Recipe) {
  //   this.recipeWasSelected.emit(recipe);
  // }
  onNewRecipeSelection() {
    this.router.navigate(['new'], { relativeTo: this.route })
  }
}
