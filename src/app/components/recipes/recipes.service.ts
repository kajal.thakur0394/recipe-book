import { EventEmitter, Injectable } from "@angular/core";
import { Ingredient } from "src/app/shared/model/ingredient.model";
import { ShoppingListService } from "../shopping-list/shopping-list.service";
import { Recipe } from "./recipe.model";

@Injectable()
export class RecipeService {
  recipeSelected = new EventEmitter<Recipe>();
  constructor(private shoppingListService: ShoppingListService) { }
  private recipes: Recipe[] = [
    new Recipe("Vadapav", "Indian burger", "https://www.cookwithmanali.com/wp-content/uploads/2018/04/Vada-Pav.jpg", [new Ingredient('Buns', 1), new Ingredient('Potatoes', 5)]),
    new Recipe("Chicken Biryani", "All time bae", "https://www.pavaniskitchen.com/wp-content/uploads/2021/02/Biryani-1.jpg", [new Ingredient('Meat', 1), new Ingredient('Rice', 3)])
  ];
  getRecipes() {
    return this.recipes.slice();
  }
  getRecipeById(index: number) {
    return this.recipes[index];
  }
  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.shoppingListService.addIngredients(ingredients);
  }
}