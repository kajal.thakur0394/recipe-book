import { Component, OnInit } from '@angular/core';
import { Ingredient } from 'src/app/shared/model/ingredient.model';
import { ShoppingListService } from './shopping-list.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
 ingredients!: Ingredient[];

  // ingredients: Ingredient[] = [
  //   new Ingredient('Potatos', 5),
  //   new Ingredient('Tomatoes', 10)
  // ];
  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit(): void {
    this.ingredients=this.shoppingListService.getIngredients();
    this.shoppingListService.ingredientsChanged.subscribe(
      (ingredients:Ingredient[])=>{
        this.ingredients=ingredients;
      }
    );
  }
  // onIngredientsAdded(ingredient: Ingredient) {
  //  this.ingredients.push(ingredient);
  //   }
}
